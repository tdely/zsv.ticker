Changes
~~~~~~~

[1.1.0] - 2020-11-08
====================
Added
-----
- Ticker.start() now accepts optional argument 'immediate' for delivering an
initial tick at second 0.

Changed
-------
- Fixed Sphinx not building docs correctly due to missing 'napoleon'
extension.
- Fixed docs conf.py pathing not importing module correctly unless installed.

Removed
-------
- Removed CI job for publish GitLab Pages. Using readthedocs.org instead.


[1.0.2] - 2020-11-07
====================
Changed
-------
- Fixed CI job for publishing to GitLab Pages.


[1.0.1] - 2020-11-07
====================
Added
-----
- Tox stuff.
- GitLab CI.
- Build directory for docs: docs/build.
- requirements.txt for docs.

Changed
-------
- Tiny formatting fix.
- Fix Pylint settings for Tox use.
- Ignore incorrect Pylint error 'unsubscriptable-object': https://github.com/PyCQA/pylint/issues/3637
- Moved docs sources into subdirectory docs/src.


[1.0.0] - 2020-11-05
====================
Added
-----
- Initial release
