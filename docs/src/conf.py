# Configuration file for the Sphinx documentation builder.
#
# For a full list of options see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
from datetime import datetime

import sphinx_rtd_theme

sys.path.insert(0, os.path.abspath("../../src"))

from zsv import ticker


# -- Project information -----------------------------------------------------

project = "zsv.ticker"
author = "Tobias Dély"
copyright = "2020-{}, {}".format(datetime.now().year, author)

version = ticker.__version__
release = ticker.__version__

# -- General configuration ---------------------------------------------------

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.intersphinx",
    "sphinx.ext.napoleon",
    "sphinx.ext.todo",
    "sphinx.ext.viewcode",
    "sphinx_autodoc_typehints",
    "sphinx_rtd_theme",
]

# templates_path = ["_templates"]
source_suffix = ".rst"
master_doc = "index"
exclude_patterns = ["_build"]
add_function_parentheses = False
add_module_names = False

# -- Options for HTML output -------------------------------------------------

htmlhelp_basename = f"{project}doc"

html_theme = "sphinx_rtd_theme"
html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]
html_theme_options = {
    "logo_only": False,
    "display_version": True,
    # ToC options
    "collapse_navigation": False,
    "sticky_navigation": True,
    "navigation_depth": 5,
    "includehidden": True,
    "titles_only": False,
}
html_show_sourcelink = False
html_show_sphinx = True
html_show_copyright = True

intersphinx_mapping = {"python": ("https://docs.python.org/3/", None)}
