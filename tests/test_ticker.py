# -*- coding: utf-8 -*-
import time
import timeit

import pytest

from zsv.ticker import Ticker


class TestTicker(object):
    @pytest.mark.timeout(2)
    def test_tick_not_started(self):
        ticker = Ticker()
        assert ticker._tick is None
        assert ticker.tick() is False

    @pytest.mark.timeout(2)
    def test_start_stop(self):
        ticker = Ticker()
        ticker.start(2)
        assert ticker._tick is not None
        ticker.stop()
        assert ticker.tick() is False

    @pytest.mark.timeout(5)
    def test_tick(self):
        ticker = Ticker()
        # Make sure the ticker doesn't tick immediately
        start = timeit.default_timer()
        ticker.start(2)
        assert ticker._tick
        assert ticker.tick()
        assert int(timeit.default_timer() - start) >= 2

    @pytest.mark.timeout(2)
    def test_start_immediate(self):
        ticker = Ticker()
        ticker.start(5, True)
        # The test will timeout before delivery if immediate doesn't work
        assert ticker.tick() is True

    @pytest.mark.timeout(5)
    def test_stop(self):
        ticker = Ticker()
        ticker.start(1)
        time.sleep(2)
        ticker.stop()
        assert ticker.tick() is False

    @pytest.mark.timeout(5)
    def test_tick_uncollected(self):
        ticker = Ticker()
        ticker.start(1)
        time.sleep(2)
        assert ticker._tick.qsize() == 1
        time.sleep(2)
        assert ticker._tick.qsize() == 1

    @pytest.mark.timeout(2)
    def test_tick_start_x2(self):
        ticker = Ticker()
        ticker.start(1)
        with pytest.raises(RuntimeError):
            ticker.start(1)

    @pytest.mark.timeout(2)
    def test_tick_stop_x3(self):
        # Ensure that the tick queue does not fill up with False
        ticker = Ticker()
        ticker.start(3)
        ticker.stop()
        ticker.stop()
        ticker.stop()
